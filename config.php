<?php 
/** Configuration */

/** If local modules are available use them instead of composer */
if(file_exists('/var/www.prod/vendor/')) {
    define('__SAMSON_VENDOR_PATH', '/var/www.prod/vendor/');
}

// Define default path to vendor dir
if (!defined('__SAMSON_VENDOR_PATH')) {
    define('__SAMSON_VENDOR_PATH', 'vendor/');
}

// If local SamsonPHP core is available - use it instead of composer version
if(file_exists(__SAMSON_VENDOR_PATH.'/samsonos/php/core/')) {
    define('__SAMSON_CORE_PATH', __SAMSON_VENDOR_PATH.'/samsonos/php/core/');
}

// Define default path to samsonos/php_core dir
if (!defined('__SAMSON_CORE_PATH')) {
    define('__SAMSON_CORE_PATH', 'vendor/samsonos/php_core/');
}

// Подключить фреймворк SamsonPHP
require( __SAMSON_CORE_PATH.'samson.php');

// Add english locale
setlocales('en');

/** Config for compressor */
class CompressorConfig extends \samson\core\Config
{
    public $__module = 'compressor';

    public $output = '/var/www.final/settle.dev/www/';
}

/** Конфигурация DEV для Deploy */
class DeployConfig extends \samson\core\Config
{
    public $__module = 'deploy';

    public $host 	= 'samsonos.com';
    public $wwwroot	= '/var/www/settle.samsonos.com/www/';
    public $username= 'vitaly';
    public $password= 'Vital29121987';
}

class HTMLConfig extends \samson\core\Config
{
    public $__module = 'html';

    public $output = 'out/';
}
