<?php
function dictionary (){
	return array(
        'en' => array(
            'Платите с Settle,<br>если не хотите ждать' => 'Pay with Settle<br>and never wait for a check',
            'Приложение позволяет быстро сделать заказ<br> и оплатить счёт в заведении.' => 'Simple app to make an order and pay for a check<br>at restaurants and bars.',
            'Сообщить мне о запуске' => 'Notify me',
            'Заказывайте в меню<br>без официанта' => 'Order from a menu<br>without a waiter',
            'Платите и уходите,<br>не дожидаясь счёта' => 'Pay and go, don’t wait<br>for a check',
            'Приложение скоро появится.<br>Оставьте электронную почту, чтобы узнать о запуске.' =>'Launching soon. Be the first to try:',
            'Ваш электронный адрес уже есть' => 'Your email already registered',
            'Электронный адрес неверен' => 'Check the email and try again',
            'Ваша электронная почта' => 'Enter your email',
            'Отправить' => 'Submit',
            'Спасибо' => 'Thank you',
            'У вас уже будет 2 $ в кошельке, если вы расскажите друзьям о Settle.' => 'You’ll get $2 if you tell friends about Settle.',
            'Поделиться' => 'SHARE',
            'P. S. Сохраняйте репост в своём таймлайне.' => 'P. S. Save the post on your timeline.',

        ),
    );
}