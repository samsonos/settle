<!doctype html>
  <!--[if lte IE 8]><html class="lteie8"><![endif]-->
  <!--[if gt IE 8]><!--><html><!--<![endif]-->
  <head>
    <title>Settle - <?php iv('title')?></title>
    <link rel="icon" href="/favicon.ico">
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
    <!--<meta name="viewport" content="initial-scale=1"/>-->
  </head>

  <body>

    <header>
      <ul class="paralax">
        <li class="parallax-layer table"></li>
      </ul>

      <img src="/img/<?php echo locale_path()?>phone.png" class="phone">

      <div class="container_12">

        <?php echo m()->output('top')?>

        <div class="clear"></div>

      </div>

    </header>

    <article class="benefits">

      <div class="container_12">

        <ul class="benefits">
          <?php echo m()->output('benefits/first')?>
          <?php echo m()->output('benefits/second')?>
          <?php echo m()->output('benefits/third')?>
        </ul>

        <div class="clear"></div>

      </div>

    </article>

    <div class="background" id="form_anchor">

      <article class="subscribe">

        <div class="container_12">

          <div class="grid_10 prefix_2">
            <h2 class="std"><?php t('Приложение скоро появится.<br>Оставьте электронную почту, чтобы узнать о запуске.')?></h2>
            <h2 class="isset error">&nbsp;<br><?php t('Ваш электронный адрес уже есть')?></h2>
            <h2 class="invalid error">&nbsp;<br><?php t('Электронный адрес неверен')?></h2>
            <form>
              <input type="text" name="email" id="email" placeholder="<?php t('Ваша электронная почта')?>">
              <button class="send"><?php t('Отправить')?></button>
            </form>
          </div>

          <div class="clear"></div>

        </div>

      </article>

      <article class="success">

        <div class="container_12">

          <div class="grid_12">
            <h2><span><?php t('Спасибо')?></span><br /><strong><?php t('У вас уже будет 2 $ в кошельке, если вы расскажите друзьям о Settle.')?></strong></h2>
            <button class="share">
              <img src="/img/share.png"> <?php t('Поделиться')?>
            </button>
            <p><?php t('P. S. Сохраняйте репост в своём таймлайне.')?></p>
          </div>

          <div class="clear"></div>

        </div>

      </article>

      <footer>

        <div class="container_12">

          <div class="grid_8 prefix_4">
            <ul class="social">
              <li>
                <a class="facebook" href="">
                  <img src="/img/icons/3-h.png">
                </a>
              </li>
              <li>
                <a class="twitter" href="">
                  <img src="/img/icons/1-h.png">
                </a>
              </li>
              <li>
                <a class="email" href="">
                  <img src="/img/icons/2-h.png">
                </a>
              </li>
            </ul>
            <div class="clear"></div>
            <div class="copyright">&copy; 2014 Settle</div>
          </div>

          <div class="clear"></div>

        </div>

      </footer>

    </div>

  </body>
</html>  