<?php
/**
 * Created by Vitaly Iegorov <egorov@samsonos.com>
 * on 15.05.14 at 14:54
 */
?>
<a href="<?php url_base()?>" class="logo grid_3">
    <img src="img/header/logo.png" alt="Settle logo">
</a>

<?php m('i18n')->render('list')?>

<div class="clear"></div>

<div class="grid_8">
    <h1><?php t('Платите с Settle,<br>если не хотите ждать')?></h1>
    <h2><?php t('Приложение позволяет быстро сделать заказ<br> и оплатить счёт в заведении.')?></h2>
    <button><?php t('Сообщить мне о запуске')?></button>
</div>