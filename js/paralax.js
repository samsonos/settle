/**
 * Created by Vitaly Egorov <egorov@samsonos.com> on 08.05.14.
 */
$(function()
{
    var clone = $('.parallax-layer').clone();

    $(window).resize(function(){
        var old = $('.parallax-layer');
        var n = clone.clone();

        old.parent().append(n);

        n.parallax({"xparallax":0.5, "mouseport":'header'});
        old.remove();
    }).resize();
});