$(document).ready ->
  form_callback('.subscribe', '.success', '.background')
  cashback('a.cashback')
  header_button_scroll('header', 'form input#email', '#form_anchor')

header_button_scroll = (header, input, anchor) ->
  button = $(header).find 'button'
  $(button).click () ->
    $('html, body').animate {scrollTop:$(anchor).offset().top+'px'}, 400, () ->
      $(input).focus()

  false

cashback = (a) ->
  i = $(a).find 'i'
  $(a).hover(() ->
    $(i).show()
  , () ->
    $(i).hide()
  )

form_callback = (form_wrap, success_wrap, bg) ->
  form = $(form_wrap).find 'form'
  email = $(form_wrap).find 'input[type="text"]'
  mail_regexp = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/g

  $(form).on 'submit', () ->
    email_value = $(email).val()
    isset_email = '1@1.ru'
    test = mail_regexp.test(email_value)
    labels = {
      h2_std: $(form_wrap).find('h2.std')
      h2_valid_error: $(form_wrap).find('h2.invalid')
      h2_isset_error: $(form_wrap).find('h2.isset')
    }
    if test && email_value != isset_email
      $(labels.h2_std).show()
      $(labels.h2_valid_error).hide()
      $(labels.h2_isset_error).hide()
      $(email).removeClass 'error'
      $(form_wrap).hide()
      $(success_wrap).show()
      $(bg).addClass 'success'
      return false
    if test && email_value == isset_email
        $(labels.h2_valid_error).hide()
        $(labels.h2_std).hide()
        $(labels.h2_isset_error).show()
        $(email).addClass 'error'
        return false
    if !test
      $(labels.h2_valid_error).show()
      $(labels.h2_std).hide()
      $(labels.h2_isset_error).hide()
      $(email).addClass 'error'
      return false
    false

